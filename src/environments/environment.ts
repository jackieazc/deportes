// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB3_d3DCfySdnW2e9mvY9O5IsrALZcJ6Zo",
    authDomain: "app2-bc45d.firebaseapp.com",
    databaseURL: "https://app2-bc45d.firebaseio.com",
    projectId: "app2-bc45d",
    storageBucket: "app2-bc45d.appspot.com",
    messagingSenderId: "77353686604",
    appId: "1:77353686604:web:c5ab607e42f9c2d8bfc6e9"
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
