import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Equipo} from './equipo';

@Injectable({
  providedIn: 'root'
})
export class EquipoService {
  private equipoCollection : AngularFirestoreCollection<Equipo>;
  private equipo : Observable<Equipo[]>;

  constructor(db: AngularFirestore) {
    this.equipoCollection = db.collection<Equipo>('equipo');
    this.equipo = this.equipoCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map( a => {
          const equipo = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...equipo}
        });
      }
    ))
  }
  getAllData() {
    return this.equipo;
  }
  getData(id:string) {
    return this.equipoCollection.doc<Equipo>(id).valueChanges();
  }  
}


