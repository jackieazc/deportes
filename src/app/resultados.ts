export interface Resultados {
    id?: string;
    equipo1: string;
    equipo2: string;
    marcador:string;
}
