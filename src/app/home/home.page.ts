import { Component, OnInit } from '@angular/core';
import {Equipo} from '../equipo';
import { DataService } from '../services/data.service';
import { EquipoService } from '../equipo.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  equipo: Equipo[];
  constructor(private equipoService: EquipoService) {}
  ngOnInit() {
    this.equipoService.getAllData().subscribe(res => {
      console.log('Equipo', res);
      this.equipo = res;
    });
  }
}
