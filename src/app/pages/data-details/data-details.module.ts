import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EquipoDetailsRoutingPageModule } from './data-details-routing.module';

import { EquipoDetailsPage } from './data-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EquipoDetailsRoutingPageModule
  ],
  declarations: [EquipoDetailsPage]
})
export class EquipoDetailsPageModule {}
