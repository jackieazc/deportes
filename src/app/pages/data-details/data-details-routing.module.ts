import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EquipoDetailsPage } from './data-details.page';

const routes: Routes = [
  {
    path: '',
    component: EquipoDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EquipoDetailsRoutingPageModule {}
