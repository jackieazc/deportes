import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { Equipo } from 'src/app/equipo';
import { EquipoService } from 'src/app/equipo.service';

@Component({
  selector: 'app-data-details',
  templateUrl: './data-details.page.html',
  styleUrls: ['./data-details.page.scss'],
})
export class EquipoDetailsPage implements OnInit {
  equiposs:Equipo = {
    ciudad: "",
    nombre: "",
    foto:""
  };
  players:Jugadores = {
    apellido: "",
    nombre: "",
    foto: "",
    equipoID: ""
  }
  jugadores: Jugadores[];
  equipoID = null;
  constructor(private route: ActivatedRoute, private nav: NavController,
    private equipoService: EquipoService, private loadingController: LoadingController) { }

  ngOnInit() {
  }
}