import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { Jugadores } from 'src/app/jugadores';
import { JugadoresService } from 'src/app/jugadores.service';

@Component({
  selector: 'app-jugadores',
  templateUrl: './jugadores.page.html',
  styleUrls: ['./jugadores.page.scss'],
})
export class JugadoresPage implements OnInit {

  players:Jugadores = {
    apellido: "",
    nombre: "",
    foto: "",
    equipoID: ""
  }
  jugadores: Jugadores[];
  equipoID = null;
  constructor(private route: ActivatedRoute, private nav: NavController,
    private jugadoresService: JugadoresService, private loadingController: LoadingController) { }*/

  ngOnInit() {
    this.equipoID = this.route.snapshot.params['equipoID'];
    this.jugadoresService.getAllData().subscribe(res => {
      console.log('Jugadores', res);
      this.jugadores = res;
    });
  }

}
