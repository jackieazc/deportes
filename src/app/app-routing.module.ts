import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'details',
    loadChildren: () => import('./pages/data-details/data-details.module').then( m => m.EquipoDetailsPageModule)
  },
  {
    path: 'details/:id',
    loadChildren: () => import('./pages/data-details/data-details.module').then( m => m.EquipoDetailsPageModule)
  },
  {
    path: 'jugadores/:id',
    loadChildren: () => import('./jugadores/jugadores.module').then( m => m.JugadoresModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
