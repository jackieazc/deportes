export interface Data {
    id?:string;
    equipo: string;
    j1foto: string;
    j2foto: string;
    j3foto: string;
    j4foto: string;
    j5foto: string;
    jugador1: string;
    jugador2: string;
    jugador3: string;
    jugador4: string;
    jugador5: string;
}
